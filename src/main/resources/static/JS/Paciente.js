
$(document).ready(inicio);

function inicio(){
	Datos();
	$("#guardarPaciente").click(guardar);
	$("#eliminarPaciente").click(eliminar);
	$("#modificarPaciente").click(modificar);
}

function Datos(){
	$.ajax({
		url: "http://localhost:8080/paciente/all",
		method:"Get",
		success:function(response){
			$("#pdatos").html("");
			response.forEach(i=>{
				$("#pdatos").append(""
				  +"<tr>"
				  	+"<td>"+ i.id + "</td>"
				  	+"<td>"+ i.nombre + "</td>"
				  	+"<td>"+ i.direccion + "</td>"
				  	+"<td><button onclick='preModificar("+i.id+");' class='btn btn-outline-warning'>Editar</button>" +
				  			"<button onclick='preEliminar("+i.id+");' class='btn btn-outline-danger ml-2'>Eliminar</button></td>"
				  	
				  +"<tr>"
				  );
			});
		},
		error:function(response){
			alert("Error en la peticion" + response); 
		}
	});
}


	function reset(){
		$("#nombre").val("");
		$("#direccion").val("");
	};
	function guardar(){
		$.ajax({
			url: "http://localhost:8080/paciente/save",
			method:"Get",
			data:{
				nombre:$("#nombre").val(),
				direccion:$("#direccion").val()
			},
			success:function(response){
				Datos();
				reset();
			},
			error:errorPeticion
		});
	}

	function preEliminar(id){
		$("#idPaciente").val(id);
		$("#modalEliminarPaciente").modal();
	}

	function eliminar(){
		var id = $("#idPaciente").val();
		$.ajax({
			url: "http://localhost:8080/paciente/delete/" + id,
			method:"Get",
			data: null,
			success: function (response){
				Datos()
			},
			error: errorPeticion
		});
	}

	function preModificar(id){
		$("#modalModificarPaciente").modal();
		$.ajax({
			url: "http://localhost:8080/paciente/getPaciente/" + id,
			method:"Get",
			success:function(response){
				$("#id").val(response.id);
				$("#nombre2").val(response.nombre);
				$("#direccion2").val(response.direccion);
			},
			error:errorPeticion
		});
	}

	function modificar(){
		var id = $("#id").val();
		$.ajax({
			url: "http://localhost:8080/paciente/update/" + id,
			method: "Get",
			data:{
				id:id,
				nombre:$("#nombre2").val(),
				direccion:$("#direccion2").val()
			},
			success: function(response){
				Datos();
			},
			error: errorPeticion
		});
	}
function errorPeticion(response) {
    alert("Error al realizar la peticion: "+response);
    console.log("Error al realizar la peticion: "+response);
}