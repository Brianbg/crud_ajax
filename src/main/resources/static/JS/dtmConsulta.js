/*Variables globales*/
let paciente = { id: 0, nombre: "" }
let doctor = { id: 0, nombre: "" }
/*Funcion JQuery inicio*/
$(document).ready(inicio);
function inicio() {
    $("#guardarConsulta").click(validar);
    /* Cargar en los input el item seleccionado */
    $("body").on('click', '.agregarPaciente', function () {
        cargarPaciente($(this).parent().parent().children('td:eq(0)').text(), $(this).parent().parent().children('td:eq(1)').text());
    });

    $("body").on('click', ".agregarDoctor", function () {
        cargarDoctor($(this).parent().parent().children('td:eq(0)').text(), $(this).parent().parent().children('td:eq(1)').text());
    })

    /* $("body").on('click', ".remover", function(){
        remove($(this).parent().parent().children('td:eq(0)').text());
    }) */
    /****************************************************************************************************** */
    $("#agregarDetalle").click(agregarDetalle);
    cargarDetalles();
    /*Funcion para mostrar datos en daratable correctamente*/
    $("#Tdetalles").DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ ",
            "zeroRecords": "No hay sintomas que mostrar.",
            "info": "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty": "",
            "infoFiltered": "(Filtrados por _MAX_ total registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Anterior",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        }
    });
    $("#Tpacientes").DataTable({
        "ajax": {
            "url": "http://localhost:8080/consulta/getPacientes",
            "method": "Get"
        },
        columns: [
            {
                data: "id",
                "width": "10%"
            },
            {
                data: "nombre",
                "width": "20%"
            },
            {
                data: "direccion",
                "width": "20%"
            },
            {
                data: "operaciones",
                "width": "40%"
            }
        ],
        "scrollY": 200,
        "language": {
            "lengthMenu": "Mostrar _MENU_ ",
            "zeroRecords": "Datos no encontrados",
            "info": "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty": "Datos no encontrados",
            "infoFiltered": "(Filtrados por _MAX_ total registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Anterior",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        }
    });

    $("#Tdoctores").DataTable({
        "ajax": {
            "url": "http://localhost:8080/consulta/getDoctor",
            "method": "Get"
        },
        columns: [{
            data: "id",
            "width": "10%"
        }, {
            data: "nombre",
            "width": "10%"
        }, {
            data: "direccion",
            "width": "10%"
        }, {
            data: "especialidad",
            "width": "10%"
        }, {
            data: "operaciones",
            "width": "10%"
        }],
        "scrollY": 200,
        "language": {
            "lengthMenu": "Mostrar _MENU_ ",
            "zeroRecords": "Datos no encontrados",
            "info": "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty": "Datos no encontrados",
            "infoFiltered": "(Filtrados por _MAX_ total registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Anterior",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        }
    });
    resetDetalles();
}
/*Agregar data a la lsita en memoria*/
function agregarDetalle() {
    $.ajax({
        url: "/consulta/agregarDetalles",
        method: "Post",
        data: {
            sintoma: $("#sintomas").val()
        },
        success: function (response) {
            console.log(response.Mensaje);
            $("#sintomas").val("");
            //success();
            cargarDetalles();
        },
        error: errorPeticion
    });
}

/* Quitar detalle de la lista */

function remove(id) {

    $.ajax({
        url: "http://localhost:8080/consulta/removerDetalles/" + id,
        method: "Post",
        success: function (response) {
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: "Detalle eliminado",
                showConfirmButton: false,
                timer: 1500
            })
            cargarDetalles();
        },
        error: errorPeticion
    });
}
/*Mostar los datos de la lista en memoria*/
function cargarDetalles() {
    $.ajax({
        url: "http://localhost:8080/consulta/detalles",
        method: "Get",
        success: function (response) {
            $("#tDetalles").html("");
            response.forEach(i => {
                $("#tDetalles").append(""
                    + "<tr>"
                    + "<td>" + i.sintoma + "</td>"
                    + "<td><button class='btn btn-danger remover'  onclick='remove(" + i.id + ")'  type='button'>Eliminar</button></td>"
                    + "</tr>"
                );
            });
        },
        error: errorPeticion
    });
}
/* Cargar datos de modal a input correspondiente */
function cargarPaciente(id, nombre) {
    paciente.id = id;
    paciente.nombre = nombre;
    $("#inpPaciente").val(nombre);
    console.log(paciente);
}

function cargarDoctor(id, nombre) {
    doctor.id = id;
    doctor.nombre = nombre;
    $("#inpDoctor").val(doctor.nombre);
}
/*************************************************************************************************/

/*Cargar datos de la tabla de detalles al guardar*/
function resetDetalles() {
    $.ajax({
        url: "http://localhost:8080/consulta/resetDetalles",
        method: "Post"
    });
}


function guardarConsulta() {
    $.ajax({
        url: "http://localhost:8080/consulta/save",
        method: "Post",
        data: {
            fecha: $("#fecha").val(),
            diagnostico: $("#diagnostico").val(),
            idDoctor: doctor.id,
            idPaciente: paciente.id
        },
        success: function (response) {
            console.log(response.Mensaje + "Guardado");
            resetDetalles();
            Swal.fire({
                icon: 'success',
                title: '¡Bien!',
                text: 'Se guardo Correctamente!',
                footer: '<button class="btn btn-success" onclick="nueva()">Nueva Consulta</button>',
                showConfirmButton: false
            })
            /**footer: '<button class="btn btn-success" id="nueva">Nueva Consulta</button>',
                 showConfirmButton: false, */
        },
        error: errorPeticion
    });
}

function nueva() {
    location.reload();
}

/* Funcion para validar los inputs */
    function validar(e){
        e.preventDefault();
        $('input').next('span').remove();

        $.ajax({
            url:"http://localhost:8080/consulta/valid",
            method:"Post",
            success : function(response){
                if (response.validated) {
                    guardarConsulta();
                }else{
                    $.each(response.errorMessage, function(key, value){
                        $('input[name=' + key + ']').after('<span class="error">' + value + '</span>');
                    });
                }
            }
        });
    }
/* function validar() {
    var fecha = $("#fecha").val();
    var diagnostico = $("#diagnostico").val();
    var doc = doctor.id;
    var pac = paciente.id;
    var datos = $("#tDetalles").val();

    if (fecha == "") {
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: 'Por favor programe una fecha.',
            showConfirmButton: false,
            timer: 1500
        })
        return false;
    } else if (doc == "") {
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: 'Seleccione un "Doctor".',
            showConfirmButton: false,
            timer: 1500
        })

        return false;
    } else if (pac == 0) {
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: 'Seleccione un "Paciente".',
            showConfirmButton: false,
            timer: 1500
        })

        return false;
    } else if (diagnostico == 0) {
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: 'Por favor escriba un diagnostico.',
            showConfirmButton: false,
            timer: 1500
        })
        return false;
    } else {
        guardarConsulta();

    }

} */

/* Metodo por si hay herror */
function errorPeticion(response) {
    alert("Error al realizar la peticion: " + response);
    console.log("Error al realizar la peticion: " + response);
}