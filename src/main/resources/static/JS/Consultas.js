
$(document).ready(inicio);

function inicio(){
	Datos();
	cargarDoctores();
	cargarPacientes();
	$("#guardarConsulta").click(guardar);
	$("#eliminarConsulta").click(eliminar);
	$("#modificarConsulta").click(modificar);
}

function reset(){

	$("#sintomas").val("");
	$("#diagnostico").val("");
	$("#doctor").val("");
	$("#paciente").val("");
}

function Datos(){
	$.ajax({
		url: "http://localhost:8080/consulta/all",
		method:"Get",
		success:function(response){
			$("#cdatos").html("");
			response.forEach(i=>{
				$("#cdatos").append(""
				  +"<tr>"
				  	+"<td>"+ i.id + "</td>"
				  	+"<td>"+ i.fecha + "</td>"
				  	+"<td>"+ i.diagnostico + "</td>"
				  	+"<td>"+ i.idDoctor.nombre + "</td>"
				  	+"<td>"+ i.paciente.nombre + "</td>"
				  	+"<td><button onclick='preModificar("+i.id+")' class='btn btn-outline-warning'>Editar</button>" +
				  		"<button onclick='preEliminar("+i.id+")' class='btn btn-outline-danger ml-2'>Eliminar</button></td>"
				  	
				  +"<tr>"
				  );
			});
		},
		error:function(response){
			alert("Error en la peticion" + response); 
		}
	});
}
	function cargarDoctores(){
		$.ajax({
			url: "http://localhost:8080/consulta/getAllDoctores",
			method:"Get",
			data: null,
			success: function(response){
				response.forEach(i=>{
					$("#doctor").append(""
					+"<option value='"+i.id+"'>" + i.nombre + "</option>"
					+"");
					$("#doctor2").append(""
					+"<option value='"+i.id+"'>" + i.nombre + "</option>"
					+"" );
				});
			},
			error: errorPeticion 
		});
	}

	function cargarPacientes(){
		$.ajax({
			url: "http://localhost:8080/consulta/getAllPacientes",
			method:"Get",
			data:null,
			success: function(response){
				response.forEach(i=>{
					$("#paciente").append(""
					+"<option value='"+i.id+"'>" + i.nombre +"</option>"
					);
					$("#paciente2").append(""
					+"<option value='"+i.id+"'>" + i.nombre +"</option>"
					);
				});
			}
		});
	}

	function guardar(){
		$.ajax({
			url: "http://localhost:8080/consulta/save",
			method:"Get",
			data:{
				fecha:$("#fecha").val(),
				diagnostico:$("#diagnostico").val(),
				idDoctor: $("#doctor").val(),
				idPaciente:$("#paciente").val()
			},
			success: function(response){
				reset();
				Datos();
			},
			error: errorPeticion
			
		});
	}

	function preEliminar(id){
		$("#modalEliminarConsulta").modal();
		$("#idConsulta").val(id);
	}

	function eliminar(){
		var id = $("#idConsulta").val();
		$.ajax({
			url: "http://localhost:8080/consulta/delete/" + id,
			method:"Get",
			data:null,
			success:function(response){
				Datos();
			},
			error:errorPeticion
		});
	}

	function preModificar(id){
		$("#modalModificarConsulta").modal();
		$.ajax({
			url: "http://localhost:8080/consulta/getConsulta/" + id,
			method:"Get",
			success:function(response){
				$("#id").val(response.id);
				$("#fecha2").val(response.fecha);
				$("#diagnostico2").val(response.diagnostico);
				$("#doctor2").val(response.idDoctor.id);
				$("#paciente2").val(response.paciente.id);
			},
			error:errorPeticion
		});
	}

	function modificar(){
		var id = $("#id").val();
		$.ajax({
			url:"http://localhost:8080/consulta/update/" + id,
			method:"Get",
			data:{
				id:id,
				fecha:$("#fecha2").val(),
				diagnostico:$("#diagnostico2").val(),
				idDoctor:$("#doctor2").val(),
				idPaciente:$("#paciente2").val()
			},
			success:function(response){
				Datos();
			},
			error:errorPeticion
		});
	}


function errorPeticion(response) {
    alert("Error al realizar la peticion: "+response);
    console.log("Error al realizar la peticion: "+response);
}