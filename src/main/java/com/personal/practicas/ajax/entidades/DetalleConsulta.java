package com.personal.practicas.ajax.entidades;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;

@Entity
public class DetalleConsulta {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String sintoma;
	@ManyToOne(fetch = FetchType.EAGER)
	private Consulta consulta;
	
	public DetalleConsulta() {
		
	}

	public DetalleConsulta(int id, String sintoma, Consulta consulta) {
		super();
		this.id = id;
		this.sintoma = sintoma;
		this.consulta = consulta;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSintoma() {
		return sintoma;
	}

	public void setSintoma(String sintoma) {
		this.sintoma = sintoma;
	}

	public Consulta getIdConsulta() {
		return consulta;
	}

	public void setIdConsulta(Consulta idConsulta) {
		this.consulta = idConsulta;
	}
	
	
}
