package com.personal.practicas.ajax.entidades;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotEmpty;



@Entity
public class Consulta {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@NotEmpty(message = "Programe una fecha")
	private Date fecha;
	@NotEmpty(message = "Escriba un diagnostico")
	private String diagnostico;
	@ManyToOne(fetch = FetchType.EAGER)
	@NotEmpty(message = "Seleccione un doctor")
	private Doctores idDoctor;
	@ManyToOne(fetch = FetchType.EAGER)
	@NotEmpty(message = "Seleccione un paciente")
	private Pacientes paciente;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "consulta")
	private List<DetalleConsulta> detalleConsulta;
	
	
	public Consulta(){
		
	}
	

	public Consulta(int id, Date fecha, String diagnostico, Doctores idDoctor, Pacientes paciente,
			List<DetalleConsulta> detalleConsulta) {
		super();
		this.id = id;
		this.fecha = fecha;
		this.diagnostico = diagnostico;
		this.idDoctor = idDoctor;
		this.paciente = paciente;
		this.detalleConsulta = detalleConsulta;
	}





	public List<DetalleConsulta> getDetalleConsulta() {
		return detalleConsulta;
	}


	public void setDetalleConsulta(List<DetalleConsulta> detalleConsulta) {
		this.detalleConsulta = detalleConsulta;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}


	public String getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	public Doctores getIdDoctor() {
		return idDoctor;
	}

	public void setIdDoctor(Doctores idDoctor) {
		this.idDoctor = idDoctor;
	}

	public Pacientes getPaciente() {
		return paciente;
	}

	public void setPaciente(Pacientes paciente) {
		this.paciente = paciente;
	}
	
	
	
}
