package com.personal.practicas.ajax.controladores;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.ReadOnlyProperty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.personal.practicas.ajax.Services.ConsultaService;
import com.personal.practicas.ajax.Services.DoctorService;
import com.personal.practicas.ajax.Services.PacienteService;
import com.personal.practicas.ajax.entidades.Consulta;
import com.personal.practicas.ajax.entidades.DetalleConsulta;
import com.personal.practicas.ajax.entidades.Doctores;
import com.personal.practicas.ajax.entidades.Pacientes;
import com.personal.practicas.ajax.response.ConsultaResponse;

@Controller
@RequestMapping(value = "consulta")
public class ConsultaController {

	@Autowired
	ConsultaService daoConsulta;

	@Autowired
	DoctorService daoDoctor;

	@Autowired
	PacienteService daoPaciente;

	/*
	 * Para capturar datos de las entidades
	 * 
	 * @GetMapping(value = "getDoctor", produces = MediaType.APPLICATION_JSON_VALUE)
	 * 
	 * @ResponseBody
	 * 
	 * @CrossOrigin public Object getDoctor() { return daoDoctor.doctoresList(); }
	 */
	
	/* Metodos para obtener datos a guardar */
	@GetMapping(value = "getPacientes", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@CrossOrigin
	public Object getPaciente() {

		List<HashMap<String, Object>> registros = new ArrayList<>();

		List<Pacientes> lista = daoPaciente.listPacientes();
		for (Pacientes paciente : lista) {
			HashMap<String, Object> hm = new HashMap<>();
			hm.put("id", paciente.getId());
			hm.put("nombre", paciente.getNombre());
			hm.put("direccion", paciente.getDireccion());
			hm.put("operaciones", ""
					+ "<button class='btn btn-primary agregarPaciente' data-dismiss='modal' type='button'>Agregar</button>"
					+ "");
			registros.add(hm);
		}
		return Collections.singletonMap("data", registros);
	}

	@GetMapping(value = "getDoctor", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@CrossOrigin
	public Object getDoctor() {
		List<HashMap<String, Object>> docregistro = new ArrayList<>();
		List<Doctores> docList = daoDoctor.doctoresList();
		
		for(Doctores doctores : docList) {
			HashMap<String, Object> hm = new HashMap<>();
			hm.put("id", doctores.getId());
			hm.put("nombre", doctores.getNombre());
			hm.put("direccion", doctores.getDireccion());
			hm.put("especialidad", doctores.getEspecialidad().getEspecialidad());
			hm.put("operaciones", ""
					+ "<button class='btn btn-primary agregarDoctor' data-dismiss='modal' type='button'>Agregar</button>"
					+ "");
			docregistro.add(hm);
		}
		return Collections.singletonMap("data", docregistro);
	}

	/* Metodos para obtener datos a guardar cierre */
	public static List<DetalleConsulta> detalles = new ArrayList<>();

	public ConsultaController() {
		detalles = new ArrayList<>();
	}

	@GetMapping(value = "index")
	public String mostrar() {
		return "views/Consultas/Consultas";
	}

	@GetMapping(value = "dtm")
	public String dtm(Model model) {
		// model.addAttribute("doctores", daoConsulta.getAllDoctores());
		// model.addAttribute("pacientes", daoConsulta.getAllPacientes());
		return "views/Consultas/dtmConsultas";
	}

	/* Metodo para agregar data a la lista en memoria */
	@PostMapping(value = "agregarDetalles", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@CrossOrigin
	public Object agregarDetalle(@RequestParam String sintoma) {
		DetalleConsulta entity = new DetalleConsulta();
		entity.setSintoma(sintoma);
		detalles.add(entity);
		return new HashMap<String, String>().put("Mensaje", "Detalle agragado correctamente");
	}
	
	@PostMapping(value = "removerDetalles/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@CrossOrigin
	public Object remove(@PathVariable int id) {
		detalles.remove(id);
		return new HashMap<String, String>().put("mensaje", "Se removio con exito");
	}

	/* Metodo para cargar datos de detalles en lista */
	@GetMapping(value = "detalles", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@CrossOrigin
	public Object getDetales() {
		return detalles;
	}

	/* Resetear la tabla de los datos al guardar el registro*/
	@PostMapping(value = "resetDetalles", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@CrossOrigin
	public Object resetDetalle() {
		detalles = new ArrayList<>();
		return "lista reseteada";
	}

	/*-------------------------------------------------------------------------------------------------------------------------------*/
	@GetMapping(value = "getConsulta/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@CrossOrigin
	public Consulta getConsulta(@PathVariable int id) {
		return daoConsulta.getIdConsulta(id);
	}

	@GetMapping(value = "getAllDoctores", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@CrossOrigin
	public List<Doctores> getDoctores() {
		return (List<Doctores>) daoConsulta.getAllDoctores();
	}

	@GetMapping(value = "getAllPacientes", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@CrossOrigin
	public List<Pacientes> getPacientes() {
		return (List<Pacientes>) daoConsulta.getAllPacientes();
	}

	@GetMapping(value = "all")
	@ResponseBody
	@CrossOrigin
	public List<Consulta> listar() {
		return daoConsulta.listConsulta();
	}
	
	/*Metodo para validar*/
	@PostMapping(value = "valid", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ConsultaResponse saveConsult(@ModelAttribute @Valid Consulta consulta, BindingResult result) {
		
		ConsultaResponse response = new ConsultaResponse();
		
		if (result.hasErrors()) {
			Map<String, String> errors =  result.getFieldErrors().stream().collect(Collectors.toMap(FieldError::getField ,FieldError::getDefaultMessage));
			response.setValidated(false);
			response.setErrorMessaje(errors);
		}else {
			response.setValidated(true);
		}
		return response;
	}

	@PostMapping(value = "save")
	@ResponseBody
	@CrossOrigin
	public HashMap<String, String> guardar(
			@RequestParam(name = "fecha") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fecha,
			@RequestParam String diagnostico, @RequestParam int idDoctor, @RequestParam int idPaciente) {
		
		/*
		 * if(fecha.equals("") && diagnostico.equals("") && idDoctor == 0 && idPaciente
		 * == 0) { HashMap<String, String> hm = new HashMap<>(); hm.put("Error",
		 * "Hay campos Vacios"); }
		 */
			HashMap<String, String> hs = new HashMap<String, String>();
			Consulta con = new Consulta();
			con.setFecha(fecha);
			con.setDiagnostico(diagnostico);
			con.setIdDoctor(daoConsulta.getIdDorctor(idDoctor));
			con.setPaciente(daoConsulta.getIdPaciente(idPaciente));
			
			for(DetalleConsulta detallesConsulta: detalles) {
				detallesConsulta.setIdConsulta(con);
			}
			
			con.setDetalleConsulta(detalles);
			
			try {
				daoConsulta.saveOrUpdate(con);
				hs.put("estado", "OK");
				hs.put("Mensaje", "Registro guardado");
				return hs;
			} catch (Exception e) {
				hs.put("estado", "No OK");
				hs.put("Mensaje", "Registro no guardado");
				return hs;
			}
		
		
	}

	@GetMapping("update/{id}")
	@ResponseBody
	@CrossOrigin
	public HashMap<String, String> actualizar(@RequestParam int id,
			@RequestParam("fecha") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fecha, @RequestParam String diagnostico,
			@RequestParam int idDoctor, @RequestParam int idPaciente) {
		HashMap<String, String> hs = new HashMap<String, String>();

		Consulta con = new Consulta();
		con.setId(id);
		con.setFecha(fecha);
		con.setDiagnostico(diagnostico);
		con.setIdDoctor(daoConsulta.getIdDorctor(idDoctor));
		con.setPaciente(daoConsulta.getIdPaciente(idPaciente));

		try {
			daoConsulta.saveOrUpdate(con);
			hs.put("estado", "OK");
			hs.put("Mensaje", "Registro Actualizado");
			return hs;
		} catch (Exception e) {
			hs.put("estado", "No OK");
			hs.put("Mensaje", "Registro no Actualizado");
			return hs;
		}
	}

	@GetMapping(value = "delete/{id}")
	@ResponseBody
	@CrossOrigin
	public HashMap<String, String> borrar(@PathVariable int id) {
		HashMap<String, String> hs = new HashMap<String, String>();
		Consulta con = daoConsulta.getIdConsulta(id);
		try {
			daoConsulta.delete(con);
			hs.put("estado", "OK");
			hs.put("Mensaje", "Registro Eliminado");
			return hs;
		} catch (Exception e) {
			hs.put("estado", "No OK");
			hs.put("Mensaje", "Registro no Eliminado");
			return hs;
		}
	}
	
	/*
	 * public static void main(String[] args) { DetalleConsulta detCon = new
	 * DetalleConsulta(); detCon.setSintoma("Sintomas");
	 * System.out.println(detCon.getSintoma()); }
	 */

}
