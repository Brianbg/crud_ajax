package com.personal.practicas.ajax.controladores;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.personal.practicas.ajax.Services.ConsultaService;
import com.personal.practicas.ajax.entidades.Consulta;
import com.personal.practicas.ajax.entidades.Doctores;
import com.personal.practicas.ajax.entidades.Pacientes;

@Controller
@RequestMapping(value = "dtmConsultas")
public class DTMConsultasController {
	
	@Autowired
	ConsultaService sConsulas;
	
	//Espacio de memoria para las consultas
	public static List<Consulta> consultas = new ArrayList<Consulta>();
	
	public DTMConsultasController() {
		//Constructor vacio?
	}
	
	
	@GetMapping(value = "index")
	public String index(Model model) {
		model.addAttribute("doctores", sConsulas.getAllDoctores());
		model.addAttribute("pacientes", sConsulas.getAllPacientes());
		//model.addAttribute("doctor", doctor);
		//model.addAttribute("paciente", paciente);
		return "views/Consultas/dtmConsultas";
	}
	
	@PostMapping(value = "cargar1")
	public String cargarEntidades(@RequestParam int doctor, @RequestParam int paciente, Model model) {
		model.addAttribute("doctor", sConsulas.getIdDorctor(doctor));
		model.addAttribute("paciente", sConsulas.getIdPaciente(paciente));
		return "redirect:/ventas/guardar";
	}
	
	@GetMapping(value = "cargar")
	public String Mostrar(Model model, @RequestParam Doctores doctor) {
		model.addAttribute("doctores", sConsulas.getAllDoctores());
		model.addAttribute("pacientes", sConsulas.getAllPacientes());
		model.addAttribute("doctor", doctor);
		consultas = new ArrayList<Consulta>();
		return new String("/dtmConsultas/cargar");
	}
	
	

}
