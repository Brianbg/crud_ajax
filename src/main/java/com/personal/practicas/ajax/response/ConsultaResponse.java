package com.personal.practicas.ajax.response;

import java.util.Map;

public class ConsultaResponse {
	
	private boolean validated;
	private Map<String, String> errorMessaje;
	
	public boolean isValidated() {
		return validated;
	}
	
	public void setValidated(boolean validated) {
		this.validated = validated;
	}

	public Map<String, String> getErrorMessaje() {
		return errorMessaje;
	}

	public void setErrorMessaje(Map<String, String> errorMessaje) {
		this.errorMessaje = errorMessaje;
	}
	
	

}
