let paciente={id:0,nombre:""}
$(document).ready(inicio);
function inicio(){
    $("#agregarDetalle").click(agregarDetalle);
    cargarDetalles();
   $(".agregarPaciente").click(agregarPaciente);
    $("#Tpacientes").DataTable({
        "ajax":{
            "url": "http://localhost:8080/consulta/getPacientes",
            "method": "Get"
        },
        columns:[
        {
            data : "id",
            "width": "10%"
        },
        {
            data : "nombre",
            "width" : "20%"
        },
        {
            data : "direccion",
            "width": "20%"
        },
        {
            data : "operaciones",
            "width" : "40%"
        }
        ],
        "scrollY" : 200,
        "language" : {
            "lengthMenu" : "Mostrar _MENU_ ",
            "zeroRecords" : "Datos no encontrados",
            "info" : "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty" : "Datos no encontrados",
            "infoFiltered" : "(Filtrados por _MAX_ total registros)",
            "search" : "Buscar:",
            "paginate" : {
                "first" : "Primero",
                "last" : "Anterior",
                "next" : "Siguiente",
                "previous" : "Anterior"
            },
        }
    });
    //resetDetalles();
}
/*Agregar data a la lsita*/
function agregarDetalle(){
    $.ajax({
        url: "/consulta/agregarDetalles",
        method:"Post",
        data:{
            sintoma:$("#sintomas").val()
        },
        success:function(response){
            console.log(response.Mensaje);
            $("#sintomas").val("");
            cargarDetalles();
        },
        error:errorPeticion
    });
}
/*Mostar los datos de la lista*/

function cargarDetalles(){
    $.ajax({
        url:"http://localhost:8080/consulta/detalles",
        method:"Get",
        success:function(response){
            $("#tDetalles").html("");
            response.forEach(i => {
                $("#tDetalles").append(""
                +"<tr>"
                    +"<td>"+ i.sintoma +"</td>"
                    +"<td><button class='btn btn-danger'>Eliminar</button></td>"
                +"</tr>"
                );
            }); 
        },
        error:errorPeticion
    });
}

    function agregarPaciente(id,nombre){
        paciente.id=id;
        paciente.nombre=nombre;
        $("#paciente").val(nombre);
    }

/* Metodo por si hay herror */
function errorPeticion(response) {
    alert("Error al realizar la peticion: "+response);
    console.log("Error al realizar la peticion: "+response);
}