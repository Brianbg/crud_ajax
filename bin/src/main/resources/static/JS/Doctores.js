
$(document).ready(inicio);

function inicio() {
	Datos();
	cargarEspecialidades();
	$("#guardarDoctor").click(guardar);
	$("#eliminarDoctor").click(eliminar);
	$("#modificarDoctor").click(modificar);
}

	function reset(){
		$("#nombre").val("");
		$("#direccion").val("");
		$("#especialidad").val("");
	}

function Datos() {
	$.ajax({
		url: "http://localhost:8080/doctores/all",
		method: "Get",
		success: function (response) {
			$("#ddatos").html("");
			response.forEach(i => {
				$("#ddatos").append(""
					+ "<tr>"
					+ "<td>" + i.id + "</td>"
					+ "<td>" + i.nombre + "</td>"
					+ "<td>" + i.direccion + "</td>"
					+ "<td>" + i.especialidad.especialidad + "</td>"
					+ "<td>"
					+"<button onclick='preModificar("+i.id+");' class='btn btn-warning ml-2'>Modificar</button>"
					+ "<button onclick='preEliminar(" + i.id + ");' class='btn btn-danger ml-2'>Eliminar</button>"
					+ "</td>"

					+ "<tr>"
				);
			});
		},
		error: function (response) {
			alert("Error en la peticion" + response);
		}
	});
}

function cargarEspecialidades() {
	$.ajax({
		url: "http://localhost:8080/doctores/allEspecialidad",
		method: "Get",
		data: null,
		success: function (response) {
			response.forEach(i => {
				$("#especialidad").append(""
					+ "<option value='" + i.id + "'>" + i.especialidad + "</option>"
					+ ""
				);

				$("#especialidad2").append(""
					+ "<option value='" + i.id + "'>" + i.especialidad + "</option>"
					+ "");
			});
		},
		error: errorPeticion
	});
}

function guardar() {
	$.ajax({
		url: "http://localhost:8080/doctores/save",
		method: "Get",
		data: {
			nombre: $("#nombre").val(),
			direccion: $("#direccion").val(),
			idEspecialidad: $("#especialidad").val()
		},
		success: function (response) {
			//alert("Registro guardado");
			reset();
			Datos();
		},
		error: errorPeticion

	});
}

function preEliminar(id) {
	$("#idDoctor").val(id);
	$("#modalEliminarDoctor").modal();
}

function eliminar() {
	var id = $("#idDoctor").val();
	$.ajax({
		url: "http://localhost:8080/doctores/delete/" + id,
		method: "Get",
		data: null,
		success: function (response) {
			//alert("Registro eliminado " + response)
			Datos();
		},
		error: errorPeticion
	});
}

function preModificar(id) {
	$("#modalModificarDoctor").modal();
	$.ajax({
		url: "http://localhost:8080/doctores/getDoctor/" + id,
		method: "Get",
		success: function (response) {
			$("#id").val(response.id);
			$("#nombre2").val(response.nombre);
			$("#direccion2").val(response.direccion);
			$("#especialidad2").val(response.especialidad.id);
		},
		error: errorPeticion
	});
}

function modificar(){
	var id=$("#id").val();
	$.ajax({
		url:"http://localhost:8080/doctores/update/"+id,
		method: "Get",
		data:{
			id:id,
			nombre:$("#nombre2").val(),
			direccion:$("#direccion2").val(),
			idEspecialidad:$("#especialidad2").val()
		},
		success: function(response){
			//alert("Registros actualizado" + response);
			Datos();
		},
		error: errorPeticion
	});
}

function errorPeticion(response) {
	alert("Error al realizar la peticion: " + response);
	console.log("Error al realizar la peticion: " + response);
}
