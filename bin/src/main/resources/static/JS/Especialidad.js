
$(document).ready(inicio);

function inicio(){
	Datos();
	$("#guardarEspecialidad").click(guardar);
	$("#eliminarEspecialidad").click(eliminar);
	$("#modificarEspecialidad").click(modificar);
}

 function reset(){
	 $("#especialidad").val("");
 }

function Datos(){
	$.ajax({
		url: "http://localhost:8080/especialidad/all",
		method:"Get",
		success:function(response){
			$("#edatos").html("");
			response.forEach(i=>{
				$("#edatos").append(""
				  +"<tr>"
				  	+"<td>"+ i.id + "</td>"
				  	+"<td>"+ i.especialidad + "</td>"
				  	+"<td><button onclick='preModificar("+i.id+");' class='btn btn-outline-warning'>Editar</button></td>"
				  	+"<td><button onclick='preEliminar("+i.id+");' class='btn btn-outline-danger ml-2'>Eliminar</button></td>"
				  +"<tr>"
				  );
			});
		},
		error:function(response){
			alert("Error en la peticion" + response); 
		}
	});
}

function errorPeticion(response) {
    alert("Error al realizar la peticion: "+response);
    console.log("Error al realizar la peticion: "+response);
}

function guardar(){
	$.ajax({
		url: "http://localhost:8080/especialidad/save",
		method:"Get",
		data:{
			especialidad:$("#especialidad").val()
		},
		success:function(response){
			//alert("Registro guardado"+ response);
			reset();
			Datos();
		},
		error:errorPeticion
	});
}

function preEliminar(id){
	$("#idEspecialidad").val(id);
	$("#modalEliminarEspecialidad").modal();
}

function eliminar(){
	var id = $("#idEspecialidad").val();
	$.ajax({
		url: "http://localhost:8080/especialidad/delete/" + id,
		method: "Get",
		data: null,
		success: function(response){
			//alert(response.mensaje);
			Datos();
		},
		 error: errorPeticion
	});
}

function preModificar(id){
	$("#modalModificarEspecialidad").modal();
	$.ajax({
		url:"http://localhost:8080/especialidad/getEspecialidad/"+id,
		method: "Get",
		success: function(response){
			$("#id").val(response.id);
			$("#especialidad2").val(response.especialidad);
		},
		error: errorPeticion
	});
}

function modificar(){
	var id = $("#id").val();
	$.ajax({
		url: "http://localhost:8080/especialidad/update/"+id,
		method: "Get",
		data: {
			id:id,
			especialidad:$("#especialidad2").val()
		},
		success:function(response){
			//alert("Registros actializados");
			Datos();
		},
		error: errorPeticion
	});
}